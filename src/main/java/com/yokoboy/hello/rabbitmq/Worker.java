package com.yokoboy.hello.rabbitmq;

import com.rabbitmq.client.*;
import java.io.IOException;

public class Worker {

    private static final String TASK_QUEUE_NAME = "task_queue";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        final Connection connection = factory.newConnection();
        final Channel channel = connection.createChannel();

        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        /*
         * Qos = quality of service (服务质量)
         * 这里参数 prefetchCount 设置为1，global参数默认是false（限制级别是这个信道下面的每一个消费者）
         *
         * 作用是：如果有多个消费者(消费同一个队列)，RabbitMQ 会遍历每个消费者，
         *        如果当前遍历到的消费者 unack 的数量是1(prefetchCount=1)，则不给改消费者推送消息，自动跳到下一个消费者
         *
         * 注意：只有在 autoAck 设为false 的时候才有作用
         */
        channel.basicQos(10);

        // 第二个参数 global 默认是 false
        // 如果是false：代表对每个消费者(consumer)进行限制
        // 如果是 true：代表对信道(channel)进行限制
        // channel.basicQos(1, false);

        final Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");

                System.out.println(" [x] Received '" + message + "'");
                try {
                    doWork(message);
                } finally {
                    System.out.println(" [x] Done");

                    /*
                     * 消息确认
                     * 参数1：deliveryTag 代表一个消息的标识
                     * 参数2：multiple false:仅对当前消息ack，true:一次性ack所有小于deliveryTag的消息
                     * http://www.rabbitmq.com/confirms.html --> Acknowledging Multiple Deliveries at Once
                     */
                    channel.basicAck(envelope.getDeliveryTag(), false);
                }
            }
        };
        // 参数2：autoAck 设为false，表示需要手动确认消息
        channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
    }

    /**
     * 逻辑是如果 参数message 有几个点就 sleep 多少秒
     */
    private static void doWork(String massage) {
        for (char ch : massage.toCharArray()) {
            if (ch == '.') {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException _ignored) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
}
