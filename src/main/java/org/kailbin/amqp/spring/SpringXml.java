package org.kailbin.amqp.spring;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by kail on 2015/12/5.
 */
public class SpringXml {

    public static void main(final String... args) throws Exception {

        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext-rabbit.xml");
        RabbitTemplate template = ctx.getBean(RabbitTemplate.class);
        template.convertAndSend("helloSpring", "asd");
        for (; ; ) {
            Object o = template.receiveAndConvert("helloSpring");
            if (o == null) {
                System.out.println("空");
                break;
            }
            System.out.println(o);
        }
        System.in.read();
        ctx.destroy();

    }
}
