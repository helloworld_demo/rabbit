package org.kailbin.amqp.spring.quickstart;

import org.kailbin.amqp.AmqpUtil;
import org.kailbin.amqp.Config;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * Created by Kail on 2015/12/4.
 */
public class QuickReceiverByGet {

    public static void main(String[] args) {

        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(Config.MQ_ADDRESS);
        connectionFactory.setPort(Config.MQ_PORT);
        connectionFactory.setUsername(Config.USER_NAME);
        connectionFactory.setPassword(Config.PASSWORD);
        connectionFactory.setVirtualHost("spring");


        RabbitTemplate template = new RabbitTemplate(connectionFactory);

        Message receive = template.receive("helloSpring");
        System.out.println(AmqpUtil.message(receive.getBody()));

        connectionFactory.destroy();

    }
}
