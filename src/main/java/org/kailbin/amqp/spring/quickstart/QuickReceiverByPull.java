package org.kailbin.amqp.spring.quickstart;

import org.kailbin.amqp.AmqpUtil;
import org.kailbin.amqp.Config;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;

/**
 * Created by Kail on 2015/12/4.
 */
public class QuickReceiverByPull {

    public static void main(String[] args) {

        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(Config.MQ_ADDRESS);
        connectionFactory.setPort(Config.MQ_PORT);
        connectionFactory.setUsername(Config.USER_NAME);
        connectionFactory.setPassword(Config.PASSWORD);
        connectionFactory.setVirtualHost("spring");


        SimpleMessageListenerContainer messageListenerContainer = new SimpleMessageListenerContainer(connectionFactory);
        messageListenerContainer.addQueueNames("helloSpring");
        messageListenerContainer.setupMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                System.out.println(AmqpUtil.message(message.getBody()));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        messageListenerContainer.start();

    }
}
