package org.kailbin.amqp.spring.quickstart;

import org.kailbin.amqp.Config;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * Created by Kail on 2015/12/4.
 */
public class QuickSender {

    public static void main(String[] args) {

        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(Config.MQ_ADDRESS);
        connectionFactory.setPort(Config.MQ_PORT);
        connectionFactory.setUsername(Config.USER_NAME);
        connectionFactory.setPassword(Config.PASSWORD);
        connectionFactory.setVirtualHost("spring");

        RabbitAdmin admin = new RabbitAdmin(connectionFactory);
        admin.declareQueue(new Queue("helloSpring2"));

        RabbitTemplate template = admin.getRabbitTemplate();

        for (int i = 0; i < 100; i++) {
            template.convertAndSend("helloSpring", "foo" + i);
        }

        connectionFactory.destroy();

    }
}
