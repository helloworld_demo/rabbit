package org.kailbin.amqp.client.rpc;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.kailbin.amqp.AmqpUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Kail on 2017/8/11.
 */
public class RpcConsumer extends DefaultConsumer {


    private int CACHE_SIZE = 100;
    private LinkedBlockingQueue<String> messageIdQueue = new LinkedBlockingQueue<>(CACHE_SIZE);
    private Map<String, String> messageIdMapping = new HashMap<>(CACHE_SIZE * 2);


    private String responseQueueName;

    RpcConsumer(Channel channel, String responseQueueName) {
        super(channel);
        this.responseQueueName = responseQueueName;
    }

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
        try {
            messageIdMapping.put(properties.getCorrelationId(), AmqpUtil.message(body));
            messageIdQueue.put(properties.getCorrelationId());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    public String call(String message) throws IOException, InterruptedException {
        //生成一个 uuid
        String messageId = UUID.randomUUID().toString();

        System.out.println(messageId + "  -  " + responseQueueName);

        AMQP.BasicProperties props = new AMQP.BasicProperties.Builder()
                .correlationId(messageId) // 设置消息的 id
                .replyTo(responseQueueName) // 设置 server 处理完之后，把消息回复到 randomQueueName 里
                .build();
        getChannel().basicPublish("", R.RPC_QUEUE_NAME, props, AmqpUtil.message(message)); // 发送到 rpc_queue 里

        while (true) {
            String repMessageId = messageIdQueue.poll(1, TimeUnit.MINUTES);// 获取并移除此队列的头部，在指定的等待时间前等待可用的元素（如果有必要）。
            if (messageId.equals(repMessageId)) {
//                System.out.println("拿到 返回");
                String result = messageIdMapping.get(repMessageId);
                messageIdMapping.remove(repMessageId);
                return result;
            } else {
//                System.out.println("没拿到 放回");
                messageIdQueue.put(repMessageId);
            }
        }
    }
}
