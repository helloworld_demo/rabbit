package org.kailbin.amqp.client.rpc;

import com.rabbitmq.client.Channel;
import org.kailbin.amqp.AmqpUtil;

public class RPCClient {

    private Channel channel;
    private RpcConsumer rpcConsumer;

    private RPCClient() throws Exception {
        channel = AmqpUtil.createChannel();

        String randomQueueName = channel.queueDeclare().getQueue(); // 生成 replyQueue ，用于接收 服务器计算完成的结果
        rpcConsumer = new RpcConsumer(channel, randomQueueName);
        channel.basicConsume(randomQueueName, true, rpcConsumer);
    }

    private String call(String message) throws Exception {
        return rpcConsumer.call(message);
    }

    public void close() throws Exception {
        AmqpUtil.close(channel);
    }

    public static void main(String[] argv) throws Exception {
        final RPCClient fibonacciRpc = new RPCClient();

        for (int i = 0; i < 100; i++) {

            final int data = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println(data + " [.] response '" + fibonacciRpc.call(data + "") + "'");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

//        fibonacciRpc.close();

    }
}
