package org.kailbin.amqp.client.rpc;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.kailbin.amqp.AmqpUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RPCServer {


    public static void main(String[] argv) throws IOException, TimeoutException {

        final Channel channel = AmqpUtil.createChannel();
        channel.queueDeclare(R.RPC_QUEUE_NAME, false, false, false, null);
        System.out.println(" [x] 等待 RPC 请求 ");

        channel.basicConsume(R.RPC_QUEUE_NAME, false, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties properties, byte[] body) throws IOException {
                System.out.println("properties.getCorrelationId() ： " + properties.getCorrelationId());
                System.out.println("properties.getReplyTo() ： " + properties.getReplyTo());

                BasicProperties replyProps = new BasicProperties.Builder().correlationId(properties.getCorrelationId()).build();
                String message = AmqpUtil.message(body);

                System.out.println(" [.] message: (" + message + ")");

                channel.basicPublish("", properties.getReplyTo(), replyProps, AmqpUtil.message(message));
                channel.basicAck(envelope.getDeliveryTag(), false);

            }
        });

    }
}
