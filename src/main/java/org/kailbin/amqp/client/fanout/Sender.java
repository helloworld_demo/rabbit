package org.kailbin.amqp.client.fanout;

import com.rabbitmq.client.Channel;
import org.kailbin.amqp.AmqpUtil;

public class Sender {

    public static void main(String[] argv) throws Exception {

        Channel channel = AmqpUtil.createChannel();

        channel.exchangeDeclare(R.EXCHANGE_NAME, AmqpUtil.ExchangeTypes.FANOUT);

        for (int i = 0; i < 10000; i++) {
            String message = String.valueOf(i);
            channel.basicPublish(R.EXCHANGE_NAME, R.ROUTE_KEY, null, message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + i + "'");
        }

        AmqpUtil.close(channel);
    }

}
