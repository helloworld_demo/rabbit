package org.kailbin.amqp.client.fanout;

import com.rabbitmq.client.*;
import org.kailbin.amqp.AmqpUtil;

import java.io.IOException;

public class Receiver {


    public static void main(String[] argv) throws Exception {

        final Channel channel = AmqpUtil.createChannel();
        channel.exchangeDeclare(R.EXCHANGE_NAME, AmqpUtil.ExchangeTypes.FANOUT);

        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, R.EXCHANGE_NAME, "");//将 exchange 和 队列绑定，fanout下routeKey 可以忽略

        System.out.println(" [*] 等待接收消息  queueName:" + queueName);

        channel.basicConsume(queueName, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                long deliveryTag = envelope.getDeliveryTag();

                System.out.println(new String(body));

                channel.basicAck(deliveryTag, false);

                System.out.println("basicAck !! ");
            }
        });
    }
}
