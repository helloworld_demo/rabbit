/**
 * Copyright (c) 2015 Jiuxing All Rights Reserved.
 */
package org.kailbin.amqp.client.topic;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.kailbin.amqp.AmqpUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ReceiverTopicAll {

    public static void main(String[] argv) throws IOException, TimeoutException {

        ReceiverTopicBase.topic("#");
    }
}
