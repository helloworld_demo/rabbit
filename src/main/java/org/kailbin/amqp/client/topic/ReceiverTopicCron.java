/**
 * Copyright (c) 2015 Jiuxing All Rights Reserved.
 */
package org.kailbin.amqp.client.topic;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ReceiverTopicCron {

    public static void main(String[] argv) throws IOException, TimeoutException {

        ReceiverTopicBase.topic("cron.*");
    }
}
