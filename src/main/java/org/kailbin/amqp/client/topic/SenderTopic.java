/**
 * Copyright (c) 2015 Jiuxing All Rights Reserved.
 */
package org.kailbin.amqp.client.topic;

import com.rabbitmq.client.Channel;
import org.kailbin.amqp.AmqpUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class SenderTopic {

    public static void main(String[] argv) throws IOException, TimeoutException {
        Channel channel = AmqpUtil.createChannel();

        channel.exchangeDeclare(R.EXCHANGE_NAME, AmqpUtil.ExchangeTypes.TOPIC);

        String[] routingKeys = {"kernel.info", "cron.warning", "auth.info", "kernel.critical"};

        for (int i = 0; i < 100000; i++) {
            String routeKey = routingKeys[i % routingKeys.length];
            String message = System.currentTimeMillis() + "  " + routeKey;

            channel.basicPublish(R.EXCHANGE_NAME, routeKey, null, AmqpUtil.message(message));
            System.out.println(" [x] Sent '" + routeKey + "':'" + message + "'");
        }

        AmqpUtil.close(channel);
    }

}
