/**
 * Copyright (c) 2015 Jiuxing All Rights Reserved.
 */
package org.kailbin.amqp.client.topic;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.kailbin.amqp.AmqpUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ReceiverTopicBase {

    public static void topic(String routeKey) throws IOException, TimeoutException {


        final Channel channel = AmqpUtil.createChannel();
        channel.exchangeDeclare(R.EXCHANGE_NAME, AmqpUtil.ExchangeTypes.TOPIC);
        String queueName = channel.queueDeclare().getQueue();

        channel.queueBind(queueName, R.EXCHANGE_NAME, routeKey);
        System.out.println(" [*] 等待接收消息  " + routeKey);


        channel.basicConsume(queueName, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                long deliveryTag = envelope.getDeliveryTag();

                System.out.println(new String(body));

                channel.basicAck(deliveryTag, false);

                System.out.println("basicAck !! ");
            }
        });

    }
}
