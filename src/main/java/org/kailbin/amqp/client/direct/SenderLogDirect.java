package org.kailbin.amqp.client.direct;

import com.rabbitmq.client.Channel;
import org.kailbin.amqp.AmqpUtil;

public class SenderLogDirect {


    public static void main(String[] argv) throws Exception {
        Channel channel = AmqpUtil.createChannel();


        channel.exchangeDeclare(R.EXCHANGE_NAME, AmqpUtil.ExchangeTypes.DIRECT);

        for (int i = 0; i < 100000; i++) {
            String routeKey = getRouteKey(i);
            String message = getMessage();

            channel.basicPublish(R.EXCHANGE_NAME, routeKey, null, AmqpUtil.message(message));
            System.out.println(" [x] Sent '" + routeKey + "':'" + message + "'");
        }


        AmqpUtil.close(channel);
    }

    private static String getRouteKey(int i) {
        if (i % 3 == 0) {
            return "error";
        }
        if (i % 2 == 0) {
            return "info";
        }
        return "warn";
    }

    private static String getMessage() {
        return String.valueOf(System.currentTimeMillis());
    }

}
