
# server

- 生产者往 exchange 丢数据，如果没有 消费者（没有队列与其绑定），数据会自动丢失


# client
- **关注 队列 与 routeKey 的绑定关系**
- 默认生成的队列名是随机的： 
    - `channel.queueDeclare().getQueue()`
        - `queueDeclare("", false, true, true, null)`
            - queue     : "" 
            - durable   : false
            - exclusive : true
            - autoDelete: true
- 一个队列可以绑定多个 routeKey


