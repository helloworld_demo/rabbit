package org.kailbin.amqp.client.direct;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.kailbin.amqp.AmqpUtil;

import java.io.IOException;

public class ReceiverLogsDirect {


    public static void main(String[] route_keys) throws Exception {
        if (null == route_keys || route_keys.length <= 0) {
            route_keys = new String[]{"info", "warn", "error"};
        }

        final Channel channel = AmqpUtil.createChannel();

        channel.exchangeDeclare(R.EXCHANGE_NAME, AmqpUtil.ExchangeTypes.DIRECT);

        String queueName = channel.queueDeclare().getQueue();
        System.out.println("queueName：" + queueName);

        //一个 queue 可以绑定多个 关键字
        for (String routeKey : route_keys) {
            channel.queueBind(queueName, R.EXCHANGE_NAME, routeKey);
        }

        System.out.println(" [*] 正在等待接收消息 ");

        channel.basicConsume(queueName, new DefaultConsumer(channel) {

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {

                String format = String.format("consumerTag: %s ; getExchange : %s ;getRoutingKey : %s ;;getDeliveryTag : %s ; message : %s ;", consumerTag, envelope.getExchange(), envelope.getRoutingKey(), envelope.getDeliveryTag(), AmqpUtil.message(body));

                System.out.println(format);

                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        });


    }
}
