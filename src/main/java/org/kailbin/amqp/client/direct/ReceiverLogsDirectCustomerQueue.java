package org.kailbin.amqp.client.direct;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.kailbin.amqp.AmqpUtil;

import java.io.IOException;

public class ReceiverLogsDirectCustomerQueue {

    static String QUEUE_NAME = "direct_queue";

    public static void main(String[] a) throws Exception {
        final Channel channel = AmqpUtil.createChannel();

        channel.exchangeDeclare(R.EXCHANGE_NAME, AmqpUtil.ExchangeTypes.DIRECT);
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        System.out.println("queueName：" + QUEUE_NAME);

        channel.queueBind(QUEUE_NAME, R.EXCHANGE_NAME, "error");
        System.out.println(" [*] 正在等待接收消息 ");

        channel.basicConsume(QUEUE_NAME, new DefaultConsumer(channel) {

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {

                String format = String.format("consumerTag: %s ; getExchange : %s ;getRoutingKey : %s ;;getDeliveryTag : %s ; message : %s ;", consumerTag, envelope.getExchange(), envelope.getRoutingKey(), envelope.getDeliveryTag(), AmqpUtil.message(body));

                System.out.println(format);

                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        });
    }
}
