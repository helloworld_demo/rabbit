/**
 * Copyright (c) 2015 Jiuxing All Rights Reserved.
 */
package org.kailbin.amqp.client.simple;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.kailbin.amqp.AmqpUtil;

import java.io.IOException;

public class ReceiverByPushNew {

    public static void main(String[] argv) throws Exception {
        final Channel channel = AmqpUtil.createChannel();

        channel.queueDeclare(R.QUEUE_NAME, false, false, false, null);

        channel.basicConsume(R.QUEUE_NAME, false, "myConsumerTag", new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                long deliveryTag = envelope.getDeliveryTag();

                System.out.println(new String(body));

                channel.basicAck(deliveryTag, false);

                System.out.println("basicAck !! ");
            }
        });

//        AmqpUtil.close(channel);

    }
}