/**
 * Copyright (c) 2015 Jiuxing All Rights Reserved.
 */
package org.kailbin.amqp.client.simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;
import org.kailbin.amqp.AmqpUtil;

import java.util.concurrent.LinkedBlockingQueue;

public class ReceiverByPush {


    public static void main(String[] argv) throws Exception {


        Channel channel = AmqpUtil.createChannel();
        channel.queueDeclare(R.QUEUE_NAME, false, false, false, null);

        QueueingConsumer consumer = new QueueingConsumer(channel, new LinkedBlockingQueue<QueueingConsumer.Delivery>(1));
        channel.basicQos(1);
        channel.basicConsume(R.QUEUE_NAME, false, consumer);

        QueueingConsumer.Delivery delivery = consumer.nextDelivery();
        String message = new String(delivery.getBody(), "UTF-8");
        System.out.println(" [x] Received '" + message + "'");

        channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);

        AmqpUtil.close(channel);

    }
}