/**
 * Copyright (c) 2015 Jiuxing All Rights Reserved.
 */
package org.kailbin.amqp.client.simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.GetResponse;
import org.kailbin.amqp.AmqpUtil;

public class ReceiverByPull {

    public static void main(String[] argv) throws Exception {
        Channel channel = AmqpUtil.createChannel();

        channel.queueDeclare(R.QUEUE_NAME, false, false, false, null);

        GetResponse response = channel.basicGet(R.QUEUE_NAME, false);
        if (null != response) {
            System.out.println(new String(response.getBody()));
            channel.basicAck(response.getEnvelope().getDeliveryTag(), false);
        }
        AmqpUtil.close(channel);
    }
}