package org.kailbin.amqp.client.simple;

import com.rabbitmq.client.Channel;
import org.kailbin.amqp.AmqpUtil;

public class Sender {

    public static void main(String[] argv) throws Exception {

        Channel channel = AmqpUtil.createChannel();

        /*
         * @param queue 队列
         * @param durable 是否持久化，如果持久化，服务重启消息不会丢失
         * @param exclusive 是否是独占的，如果是该队列只对该链接可见
         * @param autoDelete 当所有和它绑定的队列都完成了对此交换机的使用后，删掉这个交换机
         * @param arguments 其他属性
         */
        channel.queueDeclare(R.QUEUE_NAME, false, false, false, null);

        for (int i = 0; i < 10_0000; i++) {
            /*
             * @param exchange
             * @param routingKey the routing key
             * @param props other properties for the message - routing headers etc
             * @param body 消息
             */
            channel.basicPublish("", R.QUEUE_NAME, null, AmqpUtil.message(String.valueOf(i)));
        }

        AmqpUtil.close(channel);
    }
}