# 核心概念
- Broker：简单来说就是消息队列服务器实体。
- Exchange：消息交换机，它指定消息按什么规则，路由到哪个队列。
- Queue：消息队列载体，每个消息都会被投入到一个或多个队列。
- Binding：绑定，它的作用就是把exchange和queue按照路由规则绑定起来。
- Routing Key：路由关键字，exchange根据这个关键字进行消息投递。
- vhost：虚拟主机，一个broker里可以开设多个vhost，用作不同用户的权限分离。
- producer：消息生产者，就是投递消息的程序。
- consumer：消息消费者，就是接受消息的程序。
- channel：消息通道，在客户端的每个连接里，可建立多个channel，每个channel代表一个会话任务。

# 使用流程
AMQP模型中，消息在producer中产生，发送到MQ的exchange上，exchange根据配置的路由方式发到相应的Queue上，
Queue又将消息发送给consumer，消息从queue到consumer有push和pull两种方式。 消息队列的使用过程大概如下：

1. 客户端连接到消息队列服务器，打开一个channel。
2. 客户端声明一个exchange，并设置相关属性。
3. 客户端声明一个queue，并设置相关属性。
4. 客户端使用routing key，在exchange和queue之间建立好绑定关系。
5. 客户端投递消息到exchange。

exchange接收到消息后，就根据消息的key和已经设置的binding，进行消息路由，将消息投递到一个或多个队列里。 
exchange也有几个类型，完全根据key进行投递的叫做Direct交换机，例如，绑定时设置了routing key为”abc”，
那么客户端提交的消息，只有设置了key为”abc”的才会投递到队列。


# Exchange 类型
- default 默认的类型。`routingKey即为Queue的name`。 message 会被投放到和routingKey一样的Queue中。（如果此时没有对应的Queue呢？抛弃或者转发到一个特定的队列） 
- direct  （通常用来处理单播路由，当然也可以用来处理多播路由）根据Message routingkey 和 Queue本身绑定的routingkey做对应，对上了，就放到这个队列里。 消息的负载均衡是在 消费端做的，而不是Queue来做的！！！会将消息循环的分配给消费者 
- fanout （处理广播路由）广播。会忽略routingKey，发送到这个Exchange中的message会被投递到在这个Exchange中所有的Queue里。  
- topic  （处理多播路由）主题订阅模式。 发送到所有绑定这个routingKey 的Queue. 通常会用 #（hash ，可以匹配一个或者多个） 或者 *（star ，只能匹配一个） 来匹配。eg:lazy.# 代表 lazy. 后边有值的 key，可以匹配有多个.的情况*.*.rabbit 代表 匹配rabbit 前有两个 . 的单词 。
- header  根据message的header进行转发，适用于路由条件比较复杂的情况 
