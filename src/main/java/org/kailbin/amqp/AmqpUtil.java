package org.kailbin.amqp;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeoutException;

/**
 * Created by Kail on 2017/8/10.
 */
public class AmqpUtil {

    public static Charset UTF8 = Charset.forName("UTF-8");

    /**
     * @see org.springframework.amqp.core.ExchangeTypes
     * <p>
     * http://rabbitmq.mr-ping.com/AMQP/AMQP_0-9-1_Model_Explained.html
     * https://www.rabbitmq.com/tutorials/amqp-concepts.html
     * http://melin.iteye.com/blog/691265
     */
    public static class ExchangeTypes {

        /**
         * 默认的类型。`routingKey即为Queue的name`。 message 会被投放到和routingKey一样的Queue中。（如果此时没有对应的Queue呢？抛弃或者转发到一个特定的队列）
         */
        public static final String DEFAULT = "";

        /**
         * 根据Message routingkey 和 Queue本身绑定的routingkey做对应，对上了，就放到这个队列里。 消息的负载均衡是在 消费端做的，而不是Queue来做的！！！会将消息循环的分配给消费者
         */
        public static final String DIRECT = BuiltinExchangeType.DIRECT.getType();

        /**
         * 广播。会忽略routingKey，发送到这个Exchange中的message会被投递到在这个Exchange中所有的Queue里。 
         */
        public static final String FANOUT = BuiltinExchangeType.FANOUT.getType();

        /**
         * 主题订阅模式。 发送到所有绑定这个routingKey 的Queue. 通常会用 #（hash ，可以匹配一个或者多个） 或者 *（star ，只能匹配一个） 来匹配。eg:lazy.# 代表 lazy. 后边有值的 key，可以匹配有多个.的情况*.*.rabbit 代表 匹配rabbit 前有两个 . 的单词 。
         */
        public static final String TOPIC = BuiltinExchangeType.TOPIC.getType();

        /**
         * 根据message的header进行转发，适用于路由条件比较复杂的情况
         */
        public static final String HEADERS = BuiltinExchangeType.HEADERS.getType();

    }


    public static Channel createChannel() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(Config.USER_NAME);
        factory.setPassword(Config.PASSWORD);
        factory.setHost(Config.MQ_ADDRESS);
        factory.setPort(Config.MQ_PORT);
        Connection connection = factory.newConnection();
        return connection.createChannel();
    }

    public static void close(Channel channel) throws IOException, TimeoutException {
        channel.close();
        channel.getConnection().close();

        System.out.println("close !! ");
    }


    public static byte[] message(String message) {
        if (null == message) {
            return null;
        }
        return message.getBytes(UTF8);
    }

    public static String message(byte[] message) {
        if (null == message) {
            return null;
        }
        return new String(message, UTF8);
    }

}
